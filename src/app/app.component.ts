import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'HEY!!!!';
  subtitle = "Oi! you there!" ;

  message = null ;


  clickCount = 0 ;

  clickTimes = [] ;


  incrementClickCount() {
    this.clickCount = this.clickCount + 1 ;
    this.clickTimes.push(new Date()) ;
  }
}
